const { file, task, option } = require("grunt");

module.exports=function(grunt){
    grunt.initConfig{
        sass:{
            dist:{
                file:[{
                    expand: true,
                    cdw: 'css',
                    src:['*.scss'],
                    dest:'css',
                    ext:'.css'
                }]
            }
        },

        watch: {
            files:['css/*.scss'],
            tasks:['css']
        },
        browserSync: {
            dev:{
                bsFiles:{ //browser files
                    src[
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
            options:{
                watchTask: true,
                server:{
                    baseDir:'./' //directorio 
                }
            }
        }
    },
    };
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.registerTask('css',['sass']);
    grunt.registerTask('default',['browserSync','watch']);
};